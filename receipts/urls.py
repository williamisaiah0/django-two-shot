from receipts.views import receipt_list_view, create_receipt, category_list
from receipts.views import create_category, create_account
from receipts.views import account_list
from django.urls import path

urlpatterns = [
    path("", receipt_list_view, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", category_list, name="category_list"),
    path("accounts/", account_list, name="account_list"),
    path("categories/create/", create_category, name="create_category"),
    path("accounts/create/", create_account, name="create_account"),
]
